import Flutter
import UIKit

public class SwiftFlutterKountPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_kount", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterKountPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
  let uuid = UUID().uuidString
      let modifiedUuid = uuid.replacingOccurrences(of: "-", with: "")
    switch(call.method){
        case"getPlatformVersion":
            result("iOS " + UIDevice.current.systemVersion)
            break;
        case"getSessionId":
            result(modifiedUuid)
            break;
        default:
            result("Not implemented")
            break;
    }
  }
}
